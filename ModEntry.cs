﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using NVorbis;
using StardewModdingAPI;
using StardewModdingAPI.Events;
using StardewModdingAPI.Utilities;
using StardewValley;
using TimeRunsOutMusic.Settings;

namespace TimeRunsOutMusic;
internal sealed class ModEntry : Mod
{
  private ModConfig config = new();
  private System.IO.FileStream stream;

  public override void Entry(IModHelper helper)
  {
    config = Helper.ReadConfig<ModConfig>();
    LoadCustomAudio();
    helper.Events.GameLoop.TimeChanged += GameLoop_TimeChanged;
    helper.Events.GameLoop.DayStarted += GameLoop_DayStarted;
    helper.Events.Player.Warped += Player_Warped;
  }

  private void LoadCustomAudio() {
    // https://stardewvalleywiki.com/Modding:Audio#Add_custom_audio
    CueDefinition timeRunsOutAudioCue = new CueDefinition();
    timeRunsOutAudioCue.name = "timeRunsOut";
    timeRunsOutAudioCue.instanceLimit = 1;
    timeRunsOutAudioCue.limitBehavior = CueDefinition.LimitBehavior.ReplaceOldest;
    // Get the audio file and add it to a SoundEffect.
    string filePathCombined = System.IO.Path.Combine(this.Helper.DirectoryPath, "time_runs_out.ogg");
    this.Monitor.Log("TimeRunsOutMusic - audio file to load: " + filePathCombined, LogLevel.Info);
    stream = new System.IO.FileStream(filePathCombined, System.IO.FileMode.Open);
    SoundEffect audio = SoundEffect.FromStream(stream, true);
    // Setting the sound effect to the new cue.
    timeRunsOutAudioCue.SetSound(audio, Game1.audioEngine.GetCategoryIndex("Music"), false);
    // Adding the cue to the sound bank.
    Game1.soundBank.AddCue(timeRunsOutAudioCue);
  }

  private void StopCustomAudio() {
    // See: https://github.com/WeDias/StardewValley/blob/main/Game1.cs
    Game1.currentSong.Stop(AudioStopOptions.Immediate);
    Game1.soundBank.GetCue("timeRunsOut").Stop(AudioStopOptions.Immediate);
    //Game1.changeMusicTrack("none");
    //Game1.currentSong.Dispose();
    Game1.changeMusicTrack(Game1.currentLocation.getMapProperty("Music"));
  }

  private void Player_Warped(object? sender, WarpedEventArgs e)
  {
    if (!Context.IsWorldReady)
      return;

    CheckTimeRunsOutCondition();
  }

  private void GameLoop_TimeChanged(object? sender, TimeChangedEventArgs e)
  {
    if (!Context.IsWorldReady)
      return;

    CheckTimeRunsOutCondition();
  }

  private void GameLoop_DayStarted(object? sender, DayStartedEventArgs e)
  {
    // stop custom music
    if (Game1.currentLocation.getMapProperty("Music") != Game1.getMusicTrackName()) {
      this.StopCustomAudio();
    }
  }

  private void CheckTimeRunsOutCondition() {
    // Comparing against something like 
    // Game1.getTrulyDarkTime(Game1.currentLocation)) would be nice.
    // But doesn't seem to be any function for "running out of time"
    // time, so the second best thing is a config based approach.
    if (Game1.timeOfDay >= config.TimeStartsToRunOut) {
      if (Game1.getMusicTrackName() != "timeRunsOut") {
        // play custom music
        Game1.changeMusicTrack("timeRunsOut");
        // Game1.playSound("timeRunsOut");
      }
    }
  }
}
