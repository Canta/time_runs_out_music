namespace TimeRunsOutMusic.Settings;
public sealed class ModConfig
{
    public bool Enabled { get; set; } = true;

    internal const int DefaultValueTimeStartsToRunsOut = 2500;
    internal static int RestrictTime(int value)
    {
        if (value < 0600)
            return 0600;

        if (value > 2600)
            return 2600;

        return value;
    }

    private int _timeStartsToRunOut = DefaultValueTimeStartsToRunsOut;
    public int TimeStartsToRunOut
    {
        get => _timeStartsToRunOut;
        set => _timeStartsToRunOut = RestrictTime(value);
    }

}
