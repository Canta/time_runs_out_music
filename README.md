# "*Time Runs Out*" music mod for Stardew Valley



## What is it?

It's a [Stardew Valley](https://www.stardewvalley.net/) mod.


## What does it do?

It allows players to set a custom background music for the *in-game* (not real life) night situation where time is running out and going to bed is urgently needed to avoid fainting.

The base game doesn't have custom music for such a situation, nor provide any mechanism to properly customize any contextual indicators. This mod adds such a contextual indicator by means of background music.


## How to install?

1. You need to install [SMAPI](https://smapi.io/). Go read SMAPI docs about how to do that.
2. With SMAPI installed, download [this file: https://canta.com.ar/stardew_valley/TimeRunsOutMusic.latest.zip](https://canta.com.ar/stardew_valley/TimeRunsOutMusic.latest.zip)
3. That's a _zip_ file. [Unzip it inside your "mods" directory](https://stardewvalleywiki.com/Modding:Player_Guide/Getting_Started#Find_your_game_folder).

That should be it. Just run the game with the new mod, and next time the *time is running out* situation triggers you should hear the new custom music.

Alternatively, you could just build the mod directly from source code. See below in this doc the steps to do it.


## Configuring the mod for playing

1. There's a `config.json` file inside the mod directory, where you can set the time of day for the *time is running out* situation. The default value is `2500`, which means "hour 25 of the day" in Stardew Valley's internal logic. The "day" for this game goes past the `24:00 / 00:00` time mark, and the next day doesn't happen until you go to sleep. So, `2500` means `01:00 AM`, and if you want to you can change that value to something else: `2000` for `08:00 PM`, `1700` for `05:00 PM`, `930` for `09:30 AM`, and so on. But please note Stardew Valley's time goes from `06:00 AM` (`600` time value) to `02:00 AM` (`2600` time value).


2. There's an audio file inside the mod directory, called `time_runs_out_music.ogg`. You can replace it with wathever `.ogg` audio file you want.


## Authors and acknowledgment
- The author of this mod is me, [Canta](https://blog.canta.com.ar).
- It's based in [JBtheShadow's "Time freezes at night"](https://github.com/JBtheShadow/StardewValleyMods/), used as base template.
- I also needed [WeDias's Stardew Valley decompilation](https://github.com/WeDias/StardewValley), given that [community ridden mod documentation](https://stardewvalleywiki.com/Modding:Index) has variable quality depending on the issue at hand, is mostly about changing assets and not so much about programming, is somewhat chaotic, and when it comes to audio it's almost non existant. It's a shame Stardew Valley is not open source and doesn't even have some API autodocs for modders: decompiling is the obvious way to go in such cases.

# Disclaimer about compatibility:
I'm a GNU/Linux user, and this was made and works on GNU/Linux.

I'm neither user, friend, enthusiast, evangelist, customer, partner, employee, or anything other than enemy of Microsoft and their products, including Windows. So I could not care less about how to make this work on that system. Consider this repo explicitly hostile towards Microsoft. Same goes for Apple stuff: I don't use that, I see no reason at all to use that, and I see that stuff hurting people's rights all around the world, so IMHO should be avoided like a disease. If you want this working on those systems, just fork it and do with the code as you please.


# Disclamier about my relationship with Stardew Valley:
I've got Stardew Valley in 2016, thinking it would be a nice way of bonding with my wife and friends through gaming. Happily got it because [it had GNU/Linux support](https://store.steampowered.com/news/app/413150/view/5315892737483295183). I abhor .NET, and partly for that reason (learning it was made in .NET) I stopped playing it about 2017 or so. Yet my wife and friends loved it, and they don't have such political IT agendas as mine.

For years I've been thinking in doing a mod from time to time for my wife and friends, but being .NET always in the way I didn't touch it at all, not even with a long stick. This last weekend in 2024 I finally gave the idea some work hours just to make my wife a bit more happy, and luckyly it was easy enough to be able to be done in a single afternoon.

Microsoft technologies are evil. They harm societies all around the world since decades now. There are lots of alternatives: Microsoft is neither necessary nor inevitable, and it's not "the best" at anything but at lobbying and corrupting goverments. Stardew Valley people should seriously consider a change of technology: if not for this project, at least for future ones. Stardew Valley could have been open source, even libre software, but Microsoft culture is noticing in it's code obscurity and lack of proper documentation, no matter we're dealing with some of the best mind-healthy games and some of the more love-oriented developers available: what Microsoft does is neither about individual people's feelings or skills, but about the software and technology cultures they pretend to control. Using Microsoft is feeding it, even if you don't pay them a single cent. Not using Microsoft is relegate them to history's obscurity no matter what they do. Please make and use libre software. Microsoft is nobody's friend, and everybody's enemy.


## License
I do libre software, so IYAM just use it. But consider this software:

  - Has been made using bits of code from other previous software. Mostly the repos mentioned before.
  - Depends on SMAPI.
  - Is actually a _mod_, which IDK if that means _derivative work_ or what.

So, frankly, I have no idea. But if it depends on me, then the license I would choose is GPLv3.
Use at your own risk: I'm not selling anything here, it's just you downloading some code from internet and doing stuff with it, so mind your responsability on it.


## Building the code
You could also build the code yourself.
As explained before, I'm not a .NET person, so you're not asking in the best place about this. Yet, recent .NET SDKs are portable enough for the dedicated person who want to make the thing work, so it shouldn't be a big deal.
So, here are the steps for building in the mod in an Ubuntu 22.04 system:

1. Install SMAPI.
2. Clone this repo somewhere in your system.
3. Be sure to have dotnet sdk installed. I used this: `sudo apt install dotnet-sdk-7.0`.
4. Go to the repo clone directory, and run this: `dotnet build TimeRunsOutMusic.csproj`.

That's it.
It also installs/updates the mod inside your Stardew Valley directory, so you can test it right away just by running the game.
